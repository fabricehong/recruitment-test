package ch.openweb.recruitment.fabricehong.contactapi.domain.skills;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepository extends JpaRepository<Skill, String> {
    Skill findByNameAndLevel(String name, Level level);
}

