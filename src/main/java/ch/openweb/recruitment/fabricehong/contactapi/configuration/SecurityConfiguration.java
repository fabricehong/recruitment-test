package ch.openweb.recruitment.fabricehong.contactapi.configuration;

import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.Contact;
import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.ContactRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    ContactRepository contactRepository;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                    .anyRequest()
                        .authenticated()
                    .and()
                .httpBasic();

    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return username -> {
            Optional<Contact> byId = contactRepository.findById(username);
            if (byId.isPresent()) {
                String format = String.format("%s-pass", username);
                PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
                return new User(username, encoder.encode(format), Lists.newArrayList());
            }
            throw new UsernameNotFoundException(String.format("Cannot find user ''", username));
        };
    }

}
