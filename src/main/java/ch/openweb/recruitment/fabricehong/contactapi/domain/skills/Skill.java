package ch.openweb.recruitment.fabricehong.contactapi.domain.skills;

import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.Contact;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@ApiModel(description = "Skill model description")
@Table(
    uniqueConstraints=@UniqueConstraint(columnNames={"name", "level"})
)
public class Skill {
    @Id
    @ApiModelProperty(accessMode = ApiModelProperty.AccessMode.READ_ONLY)
    private String id;

    @Column(nullable = false)
    private final String name;

    @Column(nullable = false)
    @ApiModelProperty("MID for the normal case. HIGH should be used only for exceptional level")
    private final Level level;

    @ManyToMany(mappedBy = "skills")
    @JsonIgnore
    private List<Contact> contacts;

    protected Skill() {
        this(null, null, null);
    }

    public Skill(String id, String name, Level level) {
        this.id = id;
        this.name = name;
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Skill skill = (Skill) o;

        return id != null ? id.equals(skill.id) : skill.id == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("Skill[%s, %s, %s]", id, name, level);
    }


}
