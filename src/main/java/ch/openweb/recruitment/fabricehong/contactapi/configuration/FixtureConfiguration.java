package ch.openweb.recruitment.fabricehong.contactapi.configuration;

import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.Contact;
import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.ContactRepository;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Level;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Skill;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.SkillRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;
import java.util.UUID;

@Configuration
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Profile("withFixtures")
public class FixtureConfiguration {

    private final ContactRepository contactRepository;
    private final SkillRepository skillRepository;

    @Bean
    InitializingBean initializeFixtures() {
        return () -> {
            Contact contact = new Contact("fhong", "Fabrice", "Hong");
            contact.setSkills(Arrays.asList(
                    createSkill("MyBestSkill", Level.HIGH),
                    createSkill( "MySecondSkill", Level.MID)
                )

            );
            contactRepository.save(contact);
        };
    }

    private Skill createSkill(String name, Level level) {
        Skill skill = new Skill(UUID.randomUUID().toString(), name, level);
        return skill;
    }


}
