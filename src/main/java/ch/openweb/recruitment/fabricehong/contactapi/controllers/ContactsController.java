package ch.openweb.recruitment.fabricehong.contactapi.controllers;

import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.Contact;
import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.ContactRepository;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Skill;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.SkillRepository;
import com.google.common.collect.Lists;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("contacts")
@Api(tags = "/contacts", description = "Contacts")
public class ContactsController {

    private final ContactRepository contactRepository;
    private final SkillRepository skillRepository;

    @GetMapping
    @ApiOperation(value= "Get all contacts")
    public Iterable<Contact> getContacts() {
        return contactRepository.findAll();
    }

    @GetMapping(value="/{id}")
    @ApiOperation(value= "Get a contact")
    public Contact getContact(@PathVariable String id) {
        Optional<Contact> contact = this.contactRepository.findById(id);
        if (contact.isPresent()) {
            return contact.get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value="/{id}")
    @ApiOperation(value= "Delete a contact")
    @PreAuthorize("#id == authentication.principal.username")
    public void deleteContact(@PathVariable String id) {
        contactRepository.deleteById(id);
    }

    @PutMapping(value="/{id}")
    @ApiOperation(value= "Update a contact")
    @PreAuthorize("#id == authentication.principal.username")
    public void updateContact(@PathVariable String id, @Valid @RequestBody Contact contact) {
        if (contact.getUsername() != null && !id.equalsIgnoreCase(contact.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    String.format("contact id '%s' in request body doesn't match the url id '%s'", contact.getUsername(), id));
        }
        contact.setUsername(id);
        contactRepository.save(contact);
    }

    @PostMapping()
    @ApiOperation(value= "Create a contact")
    public void createContact(@Valid @RequestBody Contact contact) {
        if (contactRepository.existsById(contact.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Username '%s' already exist", contact.getUsername()));
        }
        contactRepository.save(contact);
    }

    /* contact skills */

    @GetMapping("/{id}/skills")
    @ApiOperation(value= "Get contact skills")
    public List<Skill> getContactSkills(@PathVariable String id) {
        return contactRepository.findSkillsByContactId(id);
    }

    @PostMapping("/{id}/skills")
    @ApiOperation(value= "Get contact skills")
    @PreAuthorize("#id == authentication.principal.username")
    public void createContactSkill(@PathVariable String id, @Valid @RequestBody Skill skill) {
        Skill contactSkill = this.skillRepository.findByNameAndLevel(skill.getName(), skill.getLevel());
        if (contactSkill == null) {
            contactSkill = skill;
            contactSkill.setId(UUID.randomUUID().toString());
        }
        Contact one = contactRepository.getOne(id);

        one.getSkills().add(contactSkill);
        if (contactSkill.getContacts() == null) {
            contactSkill.setContacts(Lists.newArrayList());
        }
        contactRepository.save(one);
    }
}

