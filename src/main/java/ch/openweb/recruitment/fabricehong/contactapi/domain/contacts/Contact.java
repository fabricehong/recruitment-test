package ch.openweb.recruitment.fabricehong.contactapi.domain.contacts;

import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Skill;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@ApiModel(description = "A person resource in the directory")
public class Contact {
    @Id
    @Column(unique = true, nullable = false, length = 64)
    @NotNull
    @ApiModelProperty(accessMode = ApiModelProperty.AccessMode.READ_ONLY)
    private String username;

    @Column(nullable = false, length = 64)
    @NotNull
    private final String firstname;

    @Column(nullable = false, length = 64)
    @NotNull
    @Size(max = 64)
    private final String lastname;

    @Column(length = 256)
    @Size(max = 256)
    private String address;

    @Column(length = 64)
    @Size(max = 64)
    private String email;

    @Column(length = 32)
    @Size(max = 32)
    @ApiModelProperty("Swiss telephone number only")
    private String mobilePhoneNumber;


    @ManyToMany(cascade = {
            // CascadeType.REMOVE,
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(
            name = "contact_skill",
            joinColumns = @JoinColumn(name = "contact_id", referencedColumnName = "username"),
            inverseJoinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id")
    )
    private List<Skill> skills;

    @Override
    public String toString() {
        return String.format("Contact[%s, %s, %s]", username, firstname, lastname);
    }

    protected Contact() {
        this(null, null, null);
    }

    public Contact(String username, String firstname, String lastname) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Contact contact = (Contact) o;

        return username != null ? username.equals(contact.username) : contact.username == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }
}
