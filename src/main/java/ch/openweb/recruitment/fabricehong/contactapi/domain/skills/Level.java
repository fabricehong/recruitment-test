package ch.openweb.recruitment.fabricehong.contactapi.domain.skills;

public enum Level {
    LOW, MID, HIGH
}
