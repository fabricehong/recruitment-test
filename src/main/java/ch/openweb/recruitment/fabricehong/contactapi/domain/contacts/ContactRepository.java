package ch.openweb.recruitment.fabricehong.contactapi.domain.contacts;

import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, String> {
    @Query("SELECT c.skills FROM Contact c WHERE c.username = :contactId")
    List<Skill> findSkillsByContactId(@Param("contactId") String contactId);
}
