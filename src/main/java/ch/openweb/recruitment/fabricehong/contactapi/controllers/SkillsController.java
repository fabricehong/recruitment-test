package ch.openweb.recruitment.fabricehong.contactapi.controllers;

import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Skill;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.SkillRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("skills")
@Api(tags = "/skills", description = "Skills")
public class SkillsController {

    private final SkillRepository skillRepository;

    @GetMapping
    @ApiOperation(value= "Get all skills")
    public Iterable<Skill> getSkills() {
        return skillRepository.findAll();
    }

    @GetMapping(value="/{id}")
    @ApiOperation(value= "Get a skill")
    public Skill getSkill(@PathVariable String id) {
        Optional<Skill> skill = skillRepository.findById(id);
        if (skill.isPresent()) {
            return skill.get();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value="/{id}")
    @ApiOperation(value= "Delete a skill")
    public void deleteSkill(@PathVariable String id) {
        skillRepository.deleteById(id);
    }

    @PutMapping(value="/{id}")
    @ApiOperation(value= "Update a skill")
    public void updateSkill(@PathVariable String id, @Valid @RequestBody Skill skill) {
        if (skill.getId() != null && id != skill.getId()) {
            throw new RuntimeException("skill id in request body doesn't match the url");
        }
        skill.setId(id);
        skillRepository.save(skill);
    }

    @PostMapping()
    @ApiOperation(value= "Create a skill")
    public Skill createSkill(@Valid @RequestBody Skill skill) {
        Skill postedSkill = this.skillRepository.findByNameAndLevel(skill.getName(), skill.getLevel());
        if (postedSkill == null) {
            postedSkill = skill;
            postedSkill.setId(UUID.randomUUID().toString());
            skillRepository.save(skill);
        }
        return postedSkill;
    }

}

