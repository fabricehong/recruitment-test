package ch.openweb.recruitment.fabricehong.contactapi;

import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Level;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Skill;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.SkillRepository;
import lombok.extern.java.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Log
@WithMockUser(username = "connected")
public class SkillsTests extends CrudIntegrationTests {

    @Autowired
    private SkillRepository skillRepository;

	private Skill savedSkill;

    public SkillsTests() {
        super("skills", "id");
    }

    @Before
	public void setUp() {
		this.savedSkill = this.skillRepository.save(new Skill(UUID.randomUUID().toString(), "MySkill", Level.HIGH));
	}

    @After
    public void tearDown() {
        this.skillRepository.deleteAll();
    }

    /* CONTACT LIST */

	@Test
	public void testGetSkills() throws Exception {
        performGetList(1);
    }

	/* CRUD TESTS */

    @Test
    public void testGetSkill() throws Exception {
        performGet(this.savedSkill.getId());
    }

    @Test
	public void testDeleteSkill() throws Exception {
        performDelete(this.skillRepository, this.savedSkill.getId());
	}

    @Test
    @Transactional
    public void testPutSkill() throws Exception {
        String id = this.savedSkill.getId();

        // Given
        String modifiedName = "modifiedName";
        Level modifiedLevel = Level.LOW;
        Skill modifiedSkill = new Skill(null, modifiedName, modifiedLevel);

        // When
        Skill afterModification = performPut(this.skillRepository, id, modifiedSkill);

        assertThat(afterModification, notNullValue());
        assertThat(afterModification.getName(), is(modifiedName));
        assertThat(afterModification.getLevel(), is(modifiedLevel));
    }

    @Test
    public void testPostSkill() throws Exception {
        Skill newSkill = new Skill(null, "MyOtherSkill", Level.HIGH);
        performPost(this.skillRepository, newSkill, true);
    }

    @Test
    public void testPostExistingSkill() throws Exception {
        Skill newSkill = new Skill(null, savedSkill.getName(), savedSkill.getLevel());
        performPost(this.skillRepository, newSkill, false);
    }

}
