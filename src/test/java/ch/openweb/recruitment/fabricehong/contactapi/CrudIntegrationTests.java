package ch.openweb.recruitment.fabricehong.contactapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static ch.openweb.recruitment.fabricehong.contactapi.CrudIntegrationTests.CONNECTED_USER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WithMockUser(username = CONNECTED_USER)
public class CrudIntegrationTests {


    public static final String CONNECTED_USER = "connected";

    @Autowired
    protected MockMvc mockMvc;
    protected final String ressourceName;
    private final String idProperty;

    public CrudIntegrationTests(String ressourceName, String idProperty) {
        this.ressourceName = ressourceName;
        this.idProperty = idProperty;
    }

    protected static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected <T, ID> T performPut(JpaRepository<T, ID> repository, ID id, T modifiedObject) throws Exception {
        this.mockMvc.perform(put(
                    String.format("/%s/%s", ressourceName, id)
                )
                .content(asJsonString(modifiedObject))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Then
        return repository.getOne(id);
    }

    protected void performPost(JpaRepository repository, Object newContact, boolean added) throws Exception {
        int sizeBefore = repository.findAll().size();

        // When
        this.mockMvc.perform(post(String.format("/%s", ressourceName))
                .content(asJsonString(newContact))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Then
        assertThat(repository.findAll().size(), is(sizeBefore + (added ? 1 : 0)));
    }

    protected <T, ID> void performDelete(JpaRepository<T, ID> repository, ID id) throws Exception {
        assertThat(repository.existsById(id), is(true));

        this.mockMvc.perform(delete(String.format("/%s/%s", ressourceName, id))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

        assertThat(repository.existsById(id), is(false));
    }

    protected void performGet(String id) throws Exception {
        this.mockMvc.perform(get(String.format("/%s/%s", ressourceName, id))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(String.format("$.%s", this.idProperty), is(id)));
    }

    protected void performGetList(int expectedSize) throws Exception {
        this.mockMvc.perform(get(String.format("/%s", ressourceName))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(expectedSize)));
    }
}
