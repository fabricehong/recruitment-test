package ch.openweb.recruitment.fabricehong.contactapi;

import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.Contact;
import ch.openweb.recruitment.fabricehong.contactapi.domain.contacts.ContactRepository;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Level;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.Skill;
import ch.openweb.recruitment.fabricehong.contactapi.domain.skills.SkillRepository;
import lombok.extern.java.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Log
public class ContactsTests extends CrudIntegrationTests {

    public static final String OTHER_USER = "otherUser";
    @Autowired
	private ContactRepository contactRepository;

    @Autowired
    private SkillRepository skillRepository;

	private Contact savedContact;
    private Contact secondContact;

    public ContactsTests() {
        super("contacts", "username");
    }

    @Before
	public void setUp() {
		this.savedContact = createContact(CONNECTED_USER, "Michel", "Dupont",
                new Skill(UUID.randomUUID().toString(),  "MyFirstSkill", Level.HIGH),
                new Skill(UUID.randomUUID().toString(),  "MySecondSkill", Level.MID)
                );
        this.secondContact = createContact(OTHER_USER,"OtherFirstName", "OtherLastname",
                new Skill(UUID.randomUUID().toString(),  "OtherPersonSkill1", Level.LOW),
                new Skill(UUID.randomUUID().toString(),  "OtherPersonSkill2", Level.MID)
                );
    }

    @After
    public void tearDown() {
        this.contactRepository.deleteAll();
        this.skillRepository.deleteAll();
    }

    /* CONTACT LIST */

	@Test
	public void testGetContacts() throws Exception {
        performGetList(2);
    }

	/* CRUD TESTS */

    @Test
    public void testGetContact() throws Exception {
        performGet(this.savedContact.getUsername());
    }

    @Test
	public void testDeleteContact() throws Exception {
        performDelete(this.contactRepository, this.savedContact.getUsername());
	}

    @Test
    @WithMockUser(username = OTHER_USER)
    public void testDeleteContactWithOtherUser() throws Exception {
        this.mockMvc.perform(delete(
                String.format("/%s/%s", ressourceName, this.savedContact.getUsername())
        )
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }

    @Test
    @Transactional
    public void testPutContact() throws Exception {
        String id = this.savedContact.getUsername();

        // Given
        String modifiedFirstname = "modifiedFirstname";
        String modifiedLastname = "modifiedLastname";
        Contact modifiedContact = new Contact(this.savedContact.getUsername(), modifiedFirstname, modifiedLastname);

        // When
        Contact afterModification = performPut(this.contactRepository, id, modifiedContact);


        // Then
        assertThat(afterModification, notNullValue());
        assertThat(afterModification.getFirstname(), is(modifiedFirstname));
        assertThat(afterModification.getLastname(), is(modifiedLastname));
    }

    @Test
    @WithMockUser(username = OTHER_USER)
    public void testPutContactWithOtherUser() throws Exception {
        Contact modifiedContact = new Contact(this.savedContact.getUsername(), "test", "test");

        this.mockMvc.perform(put(
                String.format("/%s/%s", ressourceName, this.savedContact.getUsername())
        )
                .content(asJsonString(modifiedContact))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }

    @Test
    public void testPostContact() throws Exception {
        Contact newContact = new Contact("username", "firstname", "lastname");
        performPost(this.contactRepository, newContact, true);
    }

    @Test
    public void testPostContactExistingUsername() throws Exception {
        this.mockMvc.perform(post(String.format("/%s", ressourceName))
                .content(asJsonString(savedContact))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400));
    }


    /* CONTACT SKILLS */

    @Test
    public void testGetContactSkills() throws Exception {
        this.mockMvc.perform(get(String.format("/contacts/%s/skills", this.savedContact.getUsername()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

    @Test
    @Transactional
    public void testPostContactUnexistingSkill() throws Exception {
        int skillNumberBefore = this.skillRepository.findAll().size();
        Skill newSkill = new Skill(null, "TotallyNewSkill", Level.HIGH);

        this.mockMvc.perform(post(String.format("/contacts/%s/skills", this.savedContact.getUsername()))
                .content(asJsonString(newSkill))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(this.skillRepository.findAll().size(), equalTo(skillNumberBefore + 1 ));
    }

    @Test
    @WithMockUser(username = OTHER_USER)
    public void testPostContactSkillWithWrongUser() throws Exception {
        Skill newSkill = new Skill(null, "TotallyNewSkill", Level.HIGH);
        this.mockMvc.perform(post(String.format("/contacts/%s/skills", this.savedContact.getUsername()))
                .content(asJsonString(newSkill))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }

    @Test
    public void testPostContactExistingSkill() throws Exception {
        int skillNumberBefore = this.skillRepository.findAll().size();
        Skill existingSkill = this.secondContact.getSkills().get(0);
        Skill skillBody = new Skill(null, existingSkill.getName(), existingSkill.getLevel());

        this.mockMvc.perform(post(String.format("/contacts/%s/skills", this.savedContact.getUsername()))
                .content(asJsonString(skillBody))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(this.skillRepository.findAll().size(), equalTo(skillNumberBefore ));
    }

    private Contact createContact(String username, String firstName, String lastName, Skill... skills) {
        Contact contact = new Contact(username, firstName, lastName);
        contact.setSkills(Arrays.asList(skills));
        contact = this.contactRepository.save(contact);
        return contact;
    }

}
