# Run
To populate the database, run with profile "withFixtures".
For doing that, pass "-Dspring.profiles.active=withFixtures" to the VM arguments.
usernames loaded with the fixtures, look at class FixtureConfiguration.

# REST API documentation
The REST API documentation is available at http://localhost:8080/swagger-ui.html once the server is started.

# Authentication
Login: contact username
password: "-pass" appended to contact user name.

To see 